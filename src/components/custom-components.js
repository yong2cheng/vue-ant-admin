import countTo from 'vue-count-to'
const install = Vue => {
    Vue.component('countTo', countTo)
}
export default install
